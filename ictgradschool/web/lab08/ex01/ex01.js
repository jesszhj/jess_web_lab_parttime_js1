"use strict";

// TODO Declare variables here


// TODO Use console.log statements to print the values of the variables to the command line.
var myFirstNumber = 3;
var myBoolean = false;
var mySecondNumber = 10.25;
var myFirstString = "Hello";
var mySecondString= "World";
var myNumber1 = myFirstNumber + 7;
var myNumber2 = mySecondNumber - myFirstNumber;
var myString1 = myFirstString + " " + mySecondString;
var myCombo1 = myFirstString + myFirstNumber;
var myCombo2 = mySecondString - mySecondNumber;

console.log("myFirstNumber = " + myFirstNumber);
console.log("mySecondNumber - myFirstNumber = "+ myNumber2);