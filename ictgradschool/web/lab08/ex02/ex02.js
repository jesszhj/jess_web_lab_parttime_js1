"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

// TODO write your answers here
// 1
if (userResponse == "X" || userResponse == "Y") { console.log("true") };

//2
if (firstName.charAt(0) == "A") { console.log("true") }
else { console.log("false") };
//3
if (singer == "Taylor Swift") { console.log("true") }
else { console.log("false") };
//4
if (year > 1978 && year != 2013) { console.log("true") }
else { console.log("false") };