"use strict";

// Provided variables.
var code = "M";

// Variables you'll be assigning to in this question.
var gender = null;
// TODO Your code for part (1) here.


if (code == "f" || code == "F") {
    gender = "Female";
} else if (code == "m" || code == "M") {
    gender = "Male";
}
else {
    gender = "Unknown"
}

// Printing the answer
console.log("Part 1: The gender is: " + gender)